/*

Memory.h

Various memory utilities

*/
#pragma once

#include <stddef.h>

#include "LLib.h"

LLIB_HEADER_START

/*
Compares two operands, ptr1 and ptr2, of the length len and returns equality
*/
FORCEINLINE LLIB_HEADER_API bool memcmp(void* ptr1, void* ptr2, size_t len)
{
	for (size_t i = 0; i < len; i++)
	{
		if ((char)((char*)ptr1)[i] != (char)((char*)ptr2)[i])
			return false;
	}
	return true;
}

/*
Template of memcmp
Compares ptr1 with ptr2, and returns equality
*/
template<class T>
FORCEINLINE LLIB_HEADER_API bool memcmp(T* ptr1, T* ptr2)
{
	for (int i = 0; i < sizeof(T); i++)
	{
		if ((char)((char*)ptr1)[i] != (char)((char*)ptr2)[i])
			return false;
	}
	return true;
}

/*
Searches memory for a pattern and returns the pointer to it
NULL if no match is found

A pattern is supplied as follows:

4F FF EF ?? ?? EF FF 43 ??

Numbers are supplied in hex, and each byte is separated by a space
The question mark means "dont care", so it can be any value

Params:
	start - Starting addr
	end - Number of bytes to parse
	len - Pattern length
	pattern - Pattern

Notes:
	pattern should be a null terminated string

*/
LLIB_API void* memsearch(void* start, size_t end, size_t len, const char* pattern);

LLIB_HEADER_END