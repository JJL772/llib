/*

List.h

List container types

*/
#pragma once

#include "LLib.h"

#include "Allocator.h"

///////////////////////
//Forward decls
class DefaultAllocator;

///////////////////////

/*
LinkedList<T, I, A>

A doubly linked list type
This isn't an extremely efficient implementation, so it is recommended that you use the Array type instead

*/
template<class T, class I = int, class _Allocator = DefaultAllocator>
class LinkedList final /* No children, sorry! */
{
private:

	typedef I index_t;
	typedef I length_t;

	class CListNode
	{
		I prev_node = 0;
		I next_node = 0;
		T value;
		bool free = true;
	};

	I m_count;

	I m_size;

	_Allocator& m_allocator;

	/*
	m_contents is the base pointer where all elements will be inserted
	m_contents[0] is the first node in the list.
	*/
	CListNode* m_contents;

	FORCEINLINE void InternalResize(length_t size)
	{
		if (m_contents) 
		{
			m_contents = (CListNode*)m_allocator.realloc(m_contents, sizeof(CListNode) * size);
			m_size = size;
		}
		else
		{
			m_contents = (CListNode*)m_allocator.malloc(sizeof(CListNode) * size);
			m_size = size;
		}
	}

public:
	LinkedList(_Allocator& allocator = DefaultAllocator())
	{
		m_allocator = allocator;

		InternalResize(1);
	}

	LinkedList(length_t length, _Allocator& allocator = DefaultAllocator())
	{
		m_allocator = allocator;

		InternalResize(length);
	}

	LinkedList(const LinkedList<T, I, _Allocator>& other)
	{
		m_allocator = other.m_allocator;

		memcpy(m_contents, other.m_contents, m_size);

		m_count = other.m_count;

		m_size = other.m_size;
	}

	LinkedList(const LinkedList<T, I, _Allocator>&& other) noexcept
	{
		m_allocator = other.m_allocator;

		m_contents = other.m_contents;

		m_count = other.m_count;

		m_size = other.m_size;

		other.m_contents = NULL;

		other.m_size = 0;

		other.m_count = 0;
	}

	~LinkedList()
	{
		m_allocator.free(m_contents);
	}

public:
	void PushBack(const T& elem)
	{
		
	}

	void PushBack(const T&& elem)
	{
		
	}

	void PopBack()
	{
		
	}

	
};