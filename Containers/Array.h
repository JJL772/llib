/*

Array.h

Various simple array structures

*/
#pragma once

#include <stddef.h>
#include <functional>

#include "Allocator.h"
#include "Iterator.h"

template<class T>
class IArray;

template<class T>
class IReadOnlyArray;

template<class T, class I = int, class _Allocator = DefaultAllocator>
class Array;

template<class T>
class IWriteableArray;

class IAllocator;
class DefaultAllocator;

/*

IReadOnlyArray

Array functions that do not modify the object

*/
template<class T>
class IReadOnlyArray
{
	typedef long long index_t;
	typedef long long length_t;
public:
	/*
	Returns true if the array contains elem
	*/
	virtual bool Contains(const T& elem) const = 0;

	/*
	Returns the maximum size of the array
	This does not equate to the number of elements, but rather the total allocated size
	*/
	virtual length_t GetSize() const = 0;

	/*
	Returns the total number of elements in the array
	*/
	virtual length_t GetCount() const = 0;

	/*
	Returns a const pointer to the elements array
	*/
	virtual const T* GetElements() const = 0;

	/*
	Returns the index of elem, or -1 if not found.
	*/
	virtual index_t IndexOf(const T& elem) const = 0;

	/*
	Returns the last index of elem, or -1 if not found
	*/
	virtual index_t LastIndexOf(const T& elem) const = 0;

	/*
	Returns a read only array of pointers to the instances of elem in the array
	*/
	virtual IReadOnlyArray<T>* GetInstancesOf(const T& elem) const = 0;

	/*
	Returns the count of elem in the array
	*/
	virtual length_t GetCountOf(const T& elem) const = 0;

	/*
	Copies the array
	*/
	virtual T* Copy() const = 0;

	/*
	Checks if this array is equal to another array
	*/
	virtual bool Equals(const IReadOnlyArray<T>& other) const = 0;

	/*
	Returns the element at the specified index
	*/
	virtual const T* GetElement(index_t index) const = 0;
};

/*

IWritableArray

Array that is writable

*/
template<class T>
class IWritableArray
{
	typedef long long index_t;
	typedef long long length_t;
public:
	/*
	Replaces _old with _new in the array
	Count is the number of instances to replae
	*/
	virtual void Replace(const T& _old, const T& _new, length_t count = 1) = 0;

	/*
	Replaces all instances of _old with _new
	*/
	virtual void ReplaceAll(const T& _old, const T& _new) = 0;

	/*
	Inserts elem into the specified index in the array, if index > size-1, elem will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	virtual void Insert(index_t index, const T& elem) = 0;

	/*
	Inserts values into the array starting at the specified index. If index > size-1, values will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	virtual void InsertRange(index_t start, const IReadOnlyArray<T>& values) = 0;

	/*
	Adds an element to the array and resizes if necessary.
	This operation is guarenteed to succeed regardless of the value of elem, but may fail if an allocation fails.
	*/
	virtual void Add(const T& elem) = 0;

	/*
	Adds a range of elements to the array, resizes if necessary. 
	*/
	virtual void AddRange(const IReadOnlyArray<T>& values) = 0;

	/*
	Removes the element at the specified index. Operation fails if index > size-1
	*/
	virtual void Remove(index_t index) = 0;

	/*
	Removes elem from the array
	*/
	virtual void Remove(const T& elem, length_t count = 1) = 0;

	/*
	Removes all instances of elem from the array
	*/
	virtual void RemoveAll(const T& elem) = 0;

	/*
	Removes the values contained in the specified array from the array
	*/
	virtual void RemoveRange(const IReadOnlyArray<T>& values) = 0;

	/*
	Sorts the array using the specified comparison function
	*/
	virtual void Sort(std::function<bool(const T&, const T&)> function) = 0;

	/*
	Reserves size for the number of elements specified in the array.
	This should be called once after the initialization of an empty array.
	This operation will not occur if called more than once or if called after the array is initialized with values.
	*/
	virtual void Reserve(length_t size) = 0;

	/*
	Resizes the array. 
	Should be called only after the array has either been initialized with values or had space reserved inside of it.
	*/
	virtual void Resize(length_t size) = 0;
};

/*

IArray

Interface that all array types should implement.
Contains read & write operations

*/
template<class T>
class IArray : public IWritableArray<T>, IReadOnlyArray<T>
{
public:

};

/*

BasicArray

An array wrapper with numerous utility functions.


*/
template<class T>
class FixedArray : public IReadOnlyArray<T>
{
private:
	const T* contents;
	size_t size;

	typedef long long index_t;
	typedef long long length_t;

public:
	/*
	Normal constructor..?
	*/
	FixedArray(const T* arr, size_t len)
	{
		contents = arr;
		size = len;
	}

	/*
	Copy constructor
	*/
	FixedArray(const FixedArray<T>& other)
	{
		contents = other.GetElements();
		size = other.GetSize();
	}

	/*
	Returns true if the array contains elem
	*/
	virtual bool Contains(const T& elem) const
	{
		for (length_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				return true;
		}
		return false;
	}

	/*
	Returns the maximum size of the array
	This does not equate to the number of elements, but rather the total allocated size
	*/
	virtual length_t GetSize() const
	{
		return size;
	}

	/*
	Returns the total number of elements in the array
	*/
	virtual length_t GetCount() const
	{
		length_t i;
		for (i = 0; contents[i]; i++);
		return i;
	}

	/*
	Returns a const pointer to the elements array
	*/
	virtual const T* GetElements() const
	{
		return contents;
	}

	/*
	Returns the index of elem, or -1 if not found.
	*/
	virtual index_t IndexOf(const T& elem) const
	{
		for (index_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				return i;
		}
	}

	/*
	Returns the last index of elem, or -1 if not found
	*/
	virtual index_t LastIndexOf(const T& elem) const
	{
		for (index_t i = size - 1; i >= 0; i++)
		{
			if (contents[i] == elem)
				return i;
		}
	}

	/*
	Returns a read only array of pointers to the instances of elem in the array
	*/
	virtual IReadOnlyArray<T> GetInstancesOf(const T& elem) const
	{

	}

	/*
	Returns the count of elem in the array
	*/
	virtual length_t GetCountOf(const T& elem) const
	{
		length_t count = 0;
		for (length_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				count++;
		}
	}

	/*
	Copies the array
	*/
	virtual T* Copy() const
	{
		void* mem = std::malloc(size * sizeof(T));
		memcpy(mem, contents, sizeof(T) * size);
		return mem;
	}

	/*
	Checks if this array is equal to another array
	This performs an exact equality check. 
	*/
	virtual bool Equals(const IReadOnlyArray<T>& other) const
	{
		if (other.GetSize() != size)
			return false;

		for (length_t i = 0; i < size; i++)
		{
			if (contents[i] != *other.GetElement(i))
				return false;
		}

		return true;
	}

	virtual const T* GetElement(index_t index) const
	{
		if (index >= size || index < 0)
			return nullptr;
		return &contents[index];
	}

	bool operator==(const IReadOnlyArray<T>& other) const
	{
		return Equals(other);
	}

	bool operator!=(const IReadOnlyArray<T>& other) const
	{
		return !Equals(other);
	}

	const T& operator[](index_t index) const
	{
		return *GetElement[index];
	}
};

LLIB_HEADER_START

/*

Array

A dynamially sized writable array

*/
template<class T, class I = int, class _Allocator = DefaultAllocator>
class Array /*: public IWritableArray<T>, IReadOnlyArray<T>*/
{
private:	
	typedef I index_t;
	typedef I length_t;

	/*
	Represents an element in the array
	*/
	template<class T>
	struct ArrayElement
	{
		bool free = true;
		T elem;
	};

	/*
	Actual contents of the array
	*/
	ArrayElement<T>* m_contents = NULL;
	
	/*
	Allocated length of the array
	*/
	length_t m_size = 0;

	/*
	First free index in the array, just so we dont have to 
	*/
	index_t m_freeindex = 0;

	/*
	Count of array elements
	*/
	length_t m_count = 0;

	/*
	Array allocator
	*/
	_Allocator m_allocator;

	/*
	Iterator should be a friend
	*/
	template<class T, class I, class _Allocator = DefaultAllocator>
	friend class ArrayIterator;

	LLib::Stack<T, I, _Allocator> m_freestack;

protected:

	FORCEINLINE ArrayElement<T>* InternalCopy() const
	{
		//We do not use the allocator because we cannot guarentee that the allocator will be used to free this copy
		ArrayElement<T>* temp = (ArrayElement<T>*)malloc(sizeof(ArrayElement<T>) * m_size);
		return (ArrayElement<T>*)memcpy(temp, m_contents, sizeof(ArrayElement<T>) * m_size);
	}

	//Returns a zeroed instance of T
	FORCEINLINE T& GetZeroedInstance(unsigned count = 1) const
	{
		//T* elem = (T*)allocator.malloc(sizeof(T) * count);
		T elem;
		memset(&elem, 0, sizeof(T));
		return elem;
	}

	FORCEINLINE void InternalInsert(index_t index, const T& val)
	{
		m_contents[index].elem = val;
		m_contents[index].free = false;
		
		if (index < m_size-1 && m_contents[index + 1].free)
			m_freeindex = index + 1;
		else
			m_freeindex = index == m_freeindex ? 0 : m_freeindex;

		m_count++;
	}

	FORCEINLINE void InternalRemove(index_t index)
	{
		m_contents[index].free = true;
		m_freeindex = index;
		m_count--;
	}

	FORCEINLINE void InternalResize(length_t newsize)
	{
		if (m_contents)
			m_contents = (ArrayElement<T>*)m_allocator.realloc(m_contents, sizeof(ArrayElement<T>) * newsize);
		else
			m_contents = (ArrayElement<T>*)m_allocator.malloc(sizeof(ArrayElement<T>) * newsize);
		
		this->m_size = newsize;
		InitializeArray(newsize-this->m_size);
	}

	FORCEINLINE void InitializeArray(index_t start = 0)
	{
		for (index_t i = start; i < m_size; i++)
			m_contents[i].free = true;
	}

	FORCEINLINE void VerifyContents() const
	{
		LLIB_ASSERT(m_contents);

		if (m_contents == NULL)
			throw new std::bad_alloc("Failed allocation.");
	}

public:

	Array(_Allocator alloc = DefaultAllocator())
	{
		m_allocator = alloc;

		InternalResize(1);

		m_freestack = LLib::Stack<T, I, _Allocator>(1, alloc);
		m_freestack.Push(0);
	}

	Array(const T* elems, length_t len, _Allocator alloc = DefaultAllocator())
	{
		m_allocator = alloc;

		m_contents = (ArrayElement<T>*)m_allocator.calloc(len, sizeof(ArrayElement<T>));

		for (length_t i = 0; i < len; i++)
		{
			m_contents[i].free = false;
			m_contents[i].elem = elems[i];
			m_count++;
		}

		m_size = len;

		m_freestack = LLib::Stack<T, I, _Allocator>(len, alloc);
	}

	Array(const Array<T, I, _Allocator>& array)
	{
		m_allocator = array.m_allocator;

		m_contents = array.InternalCopy();
		
		m_size = array.GetSize();

		m_count = array.GetCount();

		m_freestack = array.m_freestack;
	}

	Array(length_t size, _Allocator alloc = DefaultAllocator())
	{
		m_allocator = alloc;

		InternalResize(size);

		m_freestack = LLib::Stack<T, I, _Allocator>(size, alloc);
	}

	~Array()
	{
		m_allocator.free(m_contents);
	}

	/*
	Creates a new iterator
	*/
	FORCEINLINE LLib::ArrayIterator<T, I, _Allocator> CreateIterator()
	{
		VerifyContents();
		return LLib::ArrayIterator<T, I, _Allocator>(*this);
	}

	/*
	Creates a new constant iterator
	*/
	FORCEINLINE LLib::ConstArrayIterator<T, I, _Allocator> CreateConstIterator() const
	{
		VerifyContents();
		return LLib::ConstArrayIterator<T, I, _Allocator>(*this);
	}


	/*
	Replaces _old with _new in the array
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void Replace(const T& _old, const T& _new, length_t _count = 1)
	{
		LLIB_ASSERT(m_count >= 0);

		VerifyContents();

		for (length_t i = 0; i < m_size && _count > 0; i++)
		{
			if (m_contents[i].elem == _old)
			{
				m_contents[i].elem = _new;
				m_count--;
				_count--;
			}
		}
	}

	/*
	Replace all instances of _old with _new
	*/
	FORCEINLINE virtual void ReplaceAll(const T& _old, const T& _new)
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].elem == _old)
			{
				m_contents[i].elem = _new;
			}
		}
	}

	/*
	Inserts elem into the specified index in the array, if index > size-1, elem will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void Insert(index_t index, const T& elem)
	{
		LLIB_ASSERT(index > 0);

		VerifyContents();


		if (index < m_size && m_contents[index].free)
		{
			InternalInsert(index, elem);
		}
	}

	/*
	Inserts values into the array starting at the specified index. If index > size, values will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void InsertRange(index_t index, const Array<T, I, _Allocator>& values)
	{
		LLIB_ASSERT(index > 0);

		VerifyContents();


		if (index <= m_size)
		{
			length_t count1 = values.GetCount();
			length_t count2 = this->GetCount();

			if (count1 + count2 >= m_size)
			{
				this->Resize(count1 + count2);
				m_size = count1 + count2;
			}

			for (length_t i = count2; i < m_size; i++)
			{
				InternalInsert(i, values.GetElement(i));
			}
		}
	}

	/*
	Adds an element to the array and resizes if necessary.
	This operation is guarenteed to succeed regardless of the value of elem, but may fail if an allocation fails.
	*/
	FORCEINLINE virtual void Add(const T& elem)
	{
		VerifyContents();

		//Start loop at first free index
		for (length_t i = m_freeindex; i < m_size; i++)
		{
			if (m_contents[i].free)
			{
				InternalInsert(i, elem);

				return;
			}
		}

		Resize(m_size + 1);
	}

	/*
	Adds a range of elements to the array, resizes if necessary.
	*/
	FORCEINLINE virtual void AddRange(const Array<T>& values)
	{
		VerifyContents();

		index_t index = m_size;
		for (; index >= 0 && m_contents[index].free == false; index--)
			;
		InsertRange(index + 1, values);
	}

	/*
	Removes the element at the specified index. Operation fails if index > size-1
	*/
	FORCEINLINE virtual void Remove(index_t index)
	{
		LLIB_ASSERT(index >= 0);

		VerifyContents();


		if (index < m_size && m_contents[index].free == false)
		{
			InternalRemove(index);
		}
	}

	/*
	Removes elem from the array
	*/
	FORCEINLINE virtual void Remove(const T& elem, length_t count = 1)
	{
		LLIB_ASSERT(count >= 0);

		VerifyContents();


		for (length_t i = 0; i < m_size && count > 0; i++)
		{
			if (m_contents[i].elem == elem)
			{
				m_contents[i].free = true;
				count--;

				m_freeindex = i < m_freeindex ? i : m_freeindex;
			}
		}
	}

	/*
	Removes all instances of elem from the array
	*/
	FORCEINLINE virtual void RemoveAll(const T& elem)
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].elem == elem)
			{
				InternalRemove(i);
			}
		}
	}

	/*
	Removes the values contained in the specified array from the array
	*/
	FORCEINLINE virtual void RemoveRange(const Array<T, I, _Allocator>& values)
	{
		VerifyContents();

		for (length_t i = 0; i < values.GetCount(); i++)
		{
			this->RemoveAll(values.GetElement(i));
		}
	}

	/*
	Sorts the array using the specified comparison function
	*/
	FORCEINLINE virtual void Sort(std::function<bool(const T&, const T&)> function)
	{
		VerifyContents();

	}

	/*
	Reserves size for the number of elements specified in the array.
	This should be called once after the initialization of an empty array.
	This operation will not occur if called more than once or if called after the array is initialized with values.
	*/
	FORCEINLINE virtual void Reserve(length_t size)
	{
		//TODO: Remove this function
		Resize(size);

		//LLIB_ASSERT(contents);
	}

	/*
	Resizes the array.
	Should be called only after the array has either been initialized with values or had space reserved inside of it.
	*/
	FORCEINLINE virtual void Resize(length_t _size)
	{
		LLIB_ASSERT(m_size > 0);

		VerifyContents();


		if (_size > this->m_size)
		{
			InternalResize(_size);
		}

		LLIB_ASSERT(m_contents);
	}

	/*
	Returns true if the array contains elem
	*/
	FORCEINLINE virtual bool Contains(const T& elem) const
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == true && m_contents[i].elem == elem)
				return true;
		}
		return false;
	}

	/*
	Returns the maximum size of the array
	This does not equate to the number of elements, but rather the total allocated size
	*/
	FORCEINLINE virtual length_t GetSize() const noexcept
	{
		return m_size;
	}

	/*
	Returns the total number of elements in the array
	*/
	FORCEINLINE virtual length_t GetCount() const noexcept
	{
		return m_count;
	}

	/*
	Returns a const pointer to the elements array
	*/
	FORCEINLINE virtual const T* GetElements() const
	{
		//TODO: Find a better way to do this horrible thing
		return this->Copy();
	}

	/*
	Returns the index of elem, or -1 if not found.
	*/
	FORCEINLINE index_t IndexOf(const T& elem) const
	{
		VerifyContents();

		for(index_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
				return i;
		}
		return -1;
	}

	/*
	Returns the last index of elem, or -1 if not found
	*/
	FORCEINLINE index_t LastIndexOf(const T& elem) const
	{
		VerifyContents();

		for (index_t i = m_size-1; i > 0; i--)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
				return i;
		}
		return -1;
	}

	/*
	Returns a read only array of pointers to the instances of elem in the array
	*/
	FORCEINLINE virtual Array<T, I, _Allocator> GetInstancesOf(const T& elem) const
	{
		VerifyContents();

		Array<T, I, _Allocator> ret = Array<T, I>(1);

		for (index_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
			{
				ret.Add(m_contents[i].elem);
			}
		}

		return ret;
	}

	/*
	Returns the count of elem in the array
	*/
	FORCEINLINE virtual length_t GetCountOf(const T& elem) const
	{
		VerifyContents();

		length_t ecount = 0;
		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
				ecount++;
		}
		return ecount;
	}

	/*
	Copies the array
	*/
	FORCEINLINE virtual T* Copy() const
	{
		VerifyContents();

		//Allocator isn't being used to allocate this as we are releasing the pointer into the wild,
		//and the allocator of the current object isn't guaranteed to be used in the free-ing of this pointer
		T* temp = (T*)malloc(sizeof(T) * m_size);

		for (length_t i = 0; i < m_size; i++)
		{
			if(m_contents[i].free == true)
				temp[i] = m_contents[i].elem;
		}

		return temp;

	}

	/*
	Checks if this array is equal to another array
	*/
	FORCEINLINE virtual bool Equals(const Array<T, I, _Allocator> other) const
	{
		VerifyContents();

		for (index_t i = 0; i < m_size; i++)
		{
			if (other.GetElement(i) != m_contents[i].elem)
				return false;
		}
	}

	/*
	Returns the element at the specified index
	*/
	FORCEINLINE virtual T GetElement(index_t index) const
	{
		LLIB_ASSERT(index >= 0);

		VerifyContents();

		if (index < m_size && m_contents[index].free == false)
			return m_contents[index].elem;
		
		return GetZeroedInstance(1);
	}

	FORCEINLINE virtual T GetLastElement() const
	{
		VerifyContents();

		for (index_t i = m_size - 1; i >= 0; i++)
		{
			if (m_contents[i].free == false)
				return m_contents[i].elem;
		}
		return GetZeroedInstance(1);
	}

	FORCEINLINE virtual bool Empty() const noexcept
	{
		return this->m_count == 0;
	}

	FORCEINLINE virtual bool IsValid() const noexcept
	{
		return m_contents != NULL;
	}

	bool operator==(const Array<T, I, _Allocator>& other) const
	{
		return Equals(other);
	}

	bool operator!=(const Array<T, I, _Allocator>& other) const
	{
		return !Equals(other);
	}

	T& operator[](int index)
	{
		T* tmp = this->GetElement(index);
		if (tmp != NULL)
			return tmp;
		else
			return this->GetZeroedInstance(1);
	}

	void operator>>(int shift)
	{
		
	}


};

LLIB_HEADER_END