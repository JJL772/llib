project(LLib ASM_NASM C CXX)

#Enable C++, C and NASM for LLib stuff
enable_language(CXX)
enable_language(C)
enable_language(ASM_NASM)

#Search for all source files
FILE(GLOB_RECURSE Src RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
	*.cpp *.h *.c *.hpp *.s *.S *.asm *.def)

#Create a new source group
source_group(Source TREE .)

#Set NASM properties for CMAKE to use
#set(CMAKE_ASM_NASM_LINK_EXECUTABLE "ld <FLAGS> <CMAKE_ASM_NASM_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>")

#Search for source files
foreach(source IN LISTS Src)
	get_filename_component(source_path "${source}" PATH)
	string(REPLACE "/" "\\" source_path_msvc "${source_path}")
	source_group("${source_path_msvc}" FILES "${source}")
endforeach()

#Add new include directories
INCLUDE_DIRECTORIES(../../Thirdparty/Vulkan/Include
		../../Thirdparty/Protobuf/Include
		${CMAKE_CURRENT_SOURCE_DIR}
)

#Add new link directories
LINK_DIRECTORIES(../../Thirdparty/Vulkan/Lib
		../../Thirdparty/Protobuf/Lib
)

#Add the target!
add_library(LLib SHARED ${Src} LLib.def)
#add_library(LLibDLL SHARED $<TARGET_OBJECTS:LLib>)
#add_library(LLibStatic STATIC $<TARGET_OBJECTS:LLib>)

#Set extra compile definitions
target_compile_definitions(LLib PRIVATE LLIB_EXPORT=1)
#Set new target properties
set_target_properties(LLib PROPERTIES ENABLE_EXPORTS 1)
set_target_properties(LLib PROPERTIES LINKER_LANGUAGE CXX)
#Set C++ standard
set_property(TARGET LLib PROPERTY CXX_STANDARD 14)