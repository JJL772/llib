#pragma once

#include "../LLib.h"

#ifdef WINDOWS
#include <Windows.h>
#undef LoadLibrary
#else

#endif

LLIB_HEADER_START

struct LibraryInfo
{
	const char* Path;
	void* Handle;
};

LLIB_API LibraryInfo* LoadLibrary(const char* name);

LLIB_API void* GetProcAddress(const void* dl, const char* name);

LLIB_API void* GetProcAddress(const LibraryInfo* dl, const char* name);

LLIB_API void FreeLibrary(LibraryInfo* dl);

LLIB_API void FreeLibrary(void* dl);

LLIB_HEADER_END

#define LoadLibrary LoadLibraryA