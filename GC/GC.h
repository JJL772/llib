/*

GC.h

Experimental C++ garbage collector.

*/
#pragma once

#include "LLib.h"

#include "Containers/Pointer.h"
#include "Containers/Array.h"

LLIB_HEADER_START

/*

GarbageCollector

This is essentially a memory manager.
You can allocate new pointers using the AllocateManagedObject(num) function, which returns a ReferencePointer to the managed object.
When all subsequent references to the object are destroyed, the object will be deallocated during a GC pass. This can be done using other methods too.

*/
class LLIB_API GarbageCollector 
{
private:

	template<class T>
	friend class ReferencePointer;

	class GCPointer
	{
		//Actual pointer
		void* m_ptr;

		//Number of references
		uint32_t m_refs;
	};

	LLib::Array<GCPointer, size_t> m_ptrs;

private:


public:

	GarbageCollector(size_t init_block = 1000)
	{
		m_ptrs = Array<GCPointer, size_t>(init_block);
	}

	template<class T>
	T* AllocateManagedObject(size_t num);
};

LLIB_HEADER_END