#include "library.h"

#undef LoadLibrary

LLIB_SOURCEFILE

LibraryInfo * LoadLibrary(const char * name)
{
	LibraryInfo* info = new LibraryInfo();
	
#ifdef WINDOWS
	HANDLE hdll = LoadLibraryA(name);
	if (hdll != NULL)
	{
		info->Path = name;
		info->Handle = (void*)hdll;
		return NULL;
	}
	else
		return NULL;
#else
	//Implement for linux
#endif

}

void * GetProcAddress(const void * dl, const char * name)
{
#ifdef WINDOWS
	return (void*)GetProcAddress((HMODULE)dl, (LPCSTR)name);
#else

#endif
}

void * GetProcAddress(const LibraryInfo * dl, const char * name)
{
#ifdef WINDOWS
	return (void*)GetProcAddress((HMODULE)dl->Handle, (LPCSTR)name);
#else

#endif
}

void FreeLibrary(LibraryInfo * dl)
{
#ifdef WINDOWS
	FreeLibrary((HMODULE)dl->Handle);
#else

#endif 
}

void FreeLibrary(void * dl)
{
#ifdef WINDOWS
	FreeLibrary((HMODULE)dl);
#else

#endif
}
