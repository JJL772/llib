/*

Stack.h

Defines stack structures

*/
#pragma once

#include "LLib.h"

#include "Allocator.h"
#include "Array.h"

#include <exception>
#include <stdexcept>


class DefaultAllocator;

template<class T, class I = int, class _Allocator = DefaultAllocator>
class Array;

template<class T, class I = int, class _Allocator = DefaultAllocator>
class Stack
{
private:
	typedef I index_t;
	typedef I length_t;

	/*
	Contents of the stack
	*/
	T* m_contents = NULL;

	/*
	Top of the stack
	*/
	index_t m_top = 0;

	/*
	Size of the stack
	*/
	length_t m_size = 0;

	/*
	Class allocator
	*/
	_Allocator& m_allocator;

public:
	Stack(length_t size, _Allocator& alloc = DefaultAllocator())
	{
		m_allocator = alloc;

		m_contents = (T*)m_allocator.calloc(size, sizeof(T));
	}

	//Copy constructor
	Stack(const Stack<T, I, _Allocator>& other)
	{
		m_allocator = other.m_allocator;
	}

	Stack(const Stack<T, I, _Allocator>&& other) :
		Stack(std::move(other))
	{
		
	}

	Stack(const T* elems, length_t size, _Allocator& alloc = DefaultAllocator())
	{
		m_allocator = alloc;

		m_contents = (T*)m_allocator.malloc(sizeof(T) * size);

		memcpy(m_contents, elems, size);
	}

	~Stack()
	{
		m_allocator.free(m_contents);
	}

private:
	FORCEINLINE T GetZeroedInstance() const
	{
		T elem;
		memset(&elem, 0, sizeof(T));
		return elem;
	}

	FORCEINLINE void InternalResize(length_t _size)
	{
		if (m_contents)
			m_contents = (T*)m_allocator.realloc(m_contents, sizeof(T) * _size);
		else
			m_contents = (T*)m_allocator.malloc(m_contents, sizeof(T) * _size);

		if (m_contents == NULL)
			throw new std::exception("Failed allocation in Stack class at " + (size_t)this);

		this->m_size = _size;
	}

public:

	FORCEINLINE void Push(const T& elem)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		if (m_top + 1 < m_size && m_contents)
		{
			m_contents[m_top + 1] = elem;
			m_top++;
		}
	}

	FORCEINLINE void Push(const Stack<T, I, _Allocator>& other)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		if (m_top + 2 + other.m_top < m_size && m_contents)
		{
			for (index_t i = m_top + 1, b = 0; i < m_size; i++)
			{
				m_contents[i] = other.m_contents[b];
				b++;
			}
		}
	}

	FORCEINLINE void Push(const T* elems, length_t len)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		LLIB_ASSERT(elems != NULL);
		LLIB_ASSERT(len > 0);

		if (m_top + 1 + len < m_size)
		{
			for (index_t i = m_top + 1, b = 0; i < m_size; i++)
			{
				m_contents[i] = elems[b];
				b++;
			}
		}
	}

	FORCEINLINE void Push(const Array<T, I, _Allocator>& other)
	{
		Push(other.GetElements(), other.GetSize());
	}

	FORCEINLINE T Pop()
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		if (m_top < 0)
			return GetZeroedInstance();

		T _top = m_contents[m_top];

		m_top--;

		return _top;
	}

	FORCEINLINE Array<T, I, _Allocator> Pop(length_t count)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		LLIB_ASSERT(count > 0);

		Array<T, I, _Allocator> ret(count);

		if (count > m_top + 1)
			return ret;

		for (length_t i = m_top; i >= m_top - count; i++)
		{
			ret.Add(m_contents[i]);
			m_top--;
		}

		return ret;
	}

	FORCEINLINE void PopAll() noexcept
	{
		m_top = 0;
	}

	FORCEINLINE void Insert(index_t index, const T& elem)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation");

		LLIB_ASSERT(index > 0 && index <= m_top);

		if (index <= m_top && index > 0)
		{
			m_contents[index] = elem;
			return;
		}
	}

	FORCEINLINE void Insert(index_t index, const T&& elem)
	{
		Insert(std::move(elem));
	}

	FORCEINLINE T GetTop() const
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw new std::bad_alloc("[STACK] Bad allocation.");

		if (m_top < 0)
			return GetZeroedInstance();
		return m_contents[m_top];
	}

	FORCEINLINE length_t GetCount() const noexcept
	{
		return m_top + 1;
	}

	FORCEINLINE length_t GetSize() const noexcept
	{
		return m_size;
	}

	FORCEINLINE void Resize(size_t _size)
	{
		if(_size > 0 && _size > this->m_size)
			InternalResize(_size);

		LLIB_ASSERT(m_contents);

		if (m_contents == NULL)
			throw new std::exception("Failed allocation!");
	}

	FORCEINLINE bool Empty() const
	{
		return m_top == 0;
	}
};