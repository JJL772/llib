/*


sin.h

Sine implementations


*/
#pragma once

#include "LLib.h"

LLIB_HEADER_START

FORCEINLINE float m_fastsinf(float f);

FORCEINLINE double m_fastsin(double d);

FORCEINLINE float m_sinf(float f);

FORCEINLINE double m_sin(double d);

LLIB_HEADER_END