#pragma once

///////////////////////////////////
// Forward Declarations
template<class I>
class TVector4;
///////////////////////////////////

#ifdef _WINDOWS
#pragma pack(push, 1)
#endif

template<class I>
class TVector3
{
public:
	I x, y, z;

	//
	// Default constructor
	// x, y, z all set to 0
	//
	TVector3()
	{
		x = (I)0;
		y = (I)0;
		z = (I)0;
	}

	//
	// Constructor
	//
	TVector3(I x, I y, I z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	//
	// Copy constructor
	// - other = other vector
	//
	TVector3(const TVector3<I>& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
	}

	//
	// Returns the X component of this vector
	//
	I GetX() const { return x; }

	//
	// Returns the Y component of this vector
	//
	I GetY() const { return y; }

	//
	// Returns the Z component of this vector
	//
	I GetZ() const { return z; }

	//
	// Sets the X component of this vector
	//
	void SetX(I x) { this->x = x; }

	//
	// Sets the Y component of this vector
	//
	void SetY(I y) { this->y = y; }

	//
	// Sets the Z component of this vector
	//
	void SetZ(I z) { this->z = z; }

	#ifdef 0
	//
	// Generic addition operator
	//
	TVector3<I> operator+(const TVector3<I> other) const
	{
		return TVector3<I>(x + other.x, y + other.y, z + other.z);
	}

	//
	// Generic subtraction operator
	//
	TVector3<I> operator-(const TVector3 <I> other) const
	{
		return TVector3<I>(x - other.x, y - other.y, z - other.z);
	}

	//
	// Add and set operator
	//
	void operator+=(const TVector3<I>& other)
	{
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
	}

	//
	// Subtract and set operator
	//
	void operator-=(const TVector3<I>& other)
	{
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
	}

	//
	// Equality operator
	//
	bool operator==(const TVector3<I>& other) const
	{
		return (other.x == x && other.y == y && other.z == z);
	}

	//
	// Inequality operator
	//
	bool operator!=(const TVector3<I>& other) const
	{
		return (other.x != x && other.y != y && other.z != z);
	}

	#endif

	//
	// Computes the cross product between this vector and another
	//
	TVector3<I> Cross(const TVector3<I>& other) const
	{
		return TVector3<I>(y*other.z - z * other.y, z*other.x - x * other.z, x*other.y - y * other.x);
	}

	//
	// Computes the dot product between this vector and another vector
	//
	I Dot(const TVector3<I>& other) const
	{
		return (I)(x * other.x + y * other.y + z * other.z);
	}

	//
	// Computes the distance between this vector and another vector
	//
	I Distance(const TVector3<I>& other) const
	{
		return (I)sqrt((double)(((x - other.x) * (x - other.x)) + ((y - other.y)*(y - other.y)) + ((z - other.z)*(z - other.z))));
	}

	//
	// Computes the midpoint between this vector and another
	//
	TVector3<I> Midpoint(const TVector3<I>& other) const
	{
		return TVector3<I>((x + other.x) / 2, (y + other.y) / 2, (z + other.z) / 2);
	}

	//
	// Computers the magnitude of this vector
	//
	I Magnitude() const
	{
		return (I)sqrt((double)(x*x + y * y + z * z));
	}

	//
	// Returns a normalized 4 component version of this vector
	//
	TVector4<I> Normalize() const
	{
		TVector4<I> vec = TVector4<I>();

		I mag = Magnitude();

		vec.x = x / mag;
		vec.y = y / mag;
		vec.z = z / mag;
		vec.m = mag;

		return vec;
	}

	//
	// Performs a linear interpolation between this vector and another vector with the specified bias
	//
	TVector3<I> Lerp(const TVector3<I>& other, float bias) const
	{
		return (*this) * bias + other * (1.0f - bias);
	}
};

#ifdef _WINDOWS
#pragma pop
#endif 