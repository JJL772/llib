;
; math.s
;
; Defines some high-performance math functions
; These are mostly experimental, some functions are used, others are not.
;

global _m_sqrtf
global _m_sqrt
global _m_sinf
global _m_tanf
global _m_cosf
global _m_cos
global _m_sin
global _m_tan
global _m_abs
global _m_absf
global _m_rsqrtf

section .text
	
	;Computes the square root of f
	;float _m_sqrtf(float f)
	_m_sqrtf:
		;Copy low float from xmm0 to high xmm0
		movsldup xmm0, xmm0
		;Sqrt of high and low, store in xmm0
		sqrtps xmm0, xmm0

		ret

	;double _m_sqrt(double d)
	_m_sqrt:
		;Copy low double from xmm0 to high xmm0
		movddup xmm0, xmm0
		;Sqrt of high and low double in xmm0, store result in xmm0
		sqrtpd xmm0, xmm0

		ret

	;float _m_sinf(float f)
	_m_sinf:
		
		movlps [rsp+8], xmm0

		fld DWORD [rsp+8]

		fsin

		fstp DWORD [rsp+8]

		movlps xmm0, [rsp+8]

		ret

	;float _m_tanf(float f)
	_m_cosf:
		
		movlps [rsp+8], xmm0

		fld DWORD [rsp+8]

		fcos

		fstp DWORD [rsp+8]

		movlps xmm0, [rsp+8]

		ret

	;float _m_tanf(float f)
	_m_tanf:

		movlps [rsp+8], xmm0

		fld DWORD [rsp+8]

		fptan

		fstp DWORD [rsp+8]

		movlpd xmm0, [rsp+8]

		ret

	;double _m_sin(double d)
	_m_sin:
		
		movlpd [rsp+8], xmm0

		fld QWORD [rsp+8]

		fsin

		fstp QWORD [rsp+8]

		movlpd xmm0, [rsp+8]

		ret

	;double _m_cos(double d)
	_m_cos:
		
		movlpd [rsp+8], xmm0

		fld QWORD [rsp+8]

		fcos

		fstp QWORD [rsp+8]

		movlpd xmm0, [rsp+8]

		ret

	;double _m_tan(double d)
	_m_tan:

		movlpd [rsp+8], xmm0

		fld QWORD [rsp+8]

		fptan

		fstp QWORD [rsp+8]

		movlpd xmm0, [rsp+8]

		ret

	;Compute inverse square root
	;float _m_rsqrtf(float f)
	_m_rsqrtf:

		rsqrtps xmm0, xmm0

		ret

	;float _m_absf(float f)
	_m_absf:

		movd DWORD eax, xmm0

		xor DWORD eax, -2147483648

		mov DWORD [rsp+8], eax

		movss DWORD xmm0, [rsp+8]

		ret

	;double _m_abs(double d)
	_m_abs:
		
		movlps [rsp+8], xmm0

		fld QWORD [rsp+8]

		fabs

		fstp QWORD [rsp+8]

		movlps xmm0, [rsp+8]

		ret