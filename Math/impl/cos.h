/*


cos.h

Cosine implementations


*/
#pragma once

#include "LLib.h"

LLIB_HEADER_START

FORCEINLINE float m_fastcosf(float f);

FORCEINLINE double m_fastcos(double d);

FORCEINLINE float m_cosf(float f);

FORCEINLINE double m_cos(double d);

LLIB_HEADER_END