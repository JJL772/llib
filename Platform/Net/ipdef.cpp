/*

ipdef.cpp

*/
#include "ipdef.h"

#include <string.h>
#include <stdlib.h>

LLIB_SOURCEFILE

uint32_t IPv4Address::_FromString(const char * str)
{
	if (str == nullptr)
		return 0;

	int len = strlen(str);

	//allocate space for new string, be mindful of the null terminator that strlen does not account for.
	//calloc fills mem with 0 so we dont have to copy in a null terminator
	char* tmp = (char*)calloc(len + 1, sizeof(char));

	//copy to buf
	memcpy(tmp, str, sizeof(char) * len);

	int count = 0;

	//replace all "." with \0
	for(int i = 0; i < len+1; i++)
	{
		if (tmp[i] == '.') {
			tmp[i] = '\0';
			count++;
		}
	}

	//sorry but we die if there arent 3 of "." in there
	if (count < 3)
		return 0;

	//4 bytes per ip
	char bytes[4];

	//Unrolled loop
	bytes[0] = atoi(tmp);
	tmp += strlen(tmp) + 1; //add to tmp pointer each iteration. strlen gives us the number of bytes left before the \0, add 1 to put us after the \0.
	bytes[1] = atoi(tmp);
	tmp += strlen(tmp) + 1;
	bytes[2] = atoi(tmp);
	tmp += strlen(tmp) + 1;
	bytes[3] = atoi(tmp);

	//free buffer
	free(tmp);

	//Reinterpret & return
	return *(uint32_t*)&bytes;
}

IPv4Address::IPv4Address(const char * addr)
{
	m_data = _FromString(addr);
}

const char * IPv4Address::ToString() const
{
	char* data = (char*)&m_data;

	char c1[4];
	char c2[4];
	char c3[4];
	char c4[4];

	sprintf(c1, "%u", (unsigned char)data[0]);
	sprintf(c2, "%u", (unsigned char)data[1]);
	sprintf(c3, "%u", (unsigned char)data[2]);
	sprintf(c4, "%u", (unsigned char)data[3]);

	char out[20];
	strcat(out, c1);
	strcat(out, ".");
	strcat(out, c2);
	strcat(out, ".");
	strcat(out, c3);
	strcat(out, ".");
	strcat(out, c4);

	//127.0.0.1
	//xxx.xxx.xxx.xxx

	return out;
}
