/*

LLib.h

Main header for LLib, "Lettuce Library"

Compile definition summary:
	_DEBUG				Enables debug stuff, usually slower
	LLIB_EXPORT			Defined only within the project to specify that symbols are to be exported
	_SIMD_VECTORS		Enables the use of SIMD intrinsics for vector math
	_SIMD_MATRICES		Enables the use of SIMD intrinsics for matrix math
	_RDRAND_ASM			Enables the use of assembly implemented random number generators
	_MATH_ASM			Enables the use of assembly implemented functions like _m_sqrt and _m_sin, etc.

Other macro summary:
	LLIB_API			Specifies a symbol as a part of the LLIB_API. Assumes symbol is implemented in a cpp file as the 
							__declspec(dllexport)/__declspec(dllimport) specifier is added
	LLIB_HEADER_API		Specifies a symbol as a part of the LLib API, however it assumes that this symbol is implemented in a header
							Used on things like template classes
	LLIB_MATH_API		Specifies a symbol as a part of the LLib math API.
	LLIB_MATH_API_ASM	Specifies that a symbol is a part of the LLib API and implemented in assembly. The dllimport specifier is added
							when LLIB_EXPORT is disabled
	LLIB_ASSERT			Debug mode assertions
	DEBUG_NOEXCEPT		Defines noexcept specifier unless in debug mode
	LLIB_NAMESPACE		Name of the LLib namespace
	LLIB_HEADER_START	Add to the beginning of header files to include its contents in the LLib namespace
	LLIB_HEADER_END		Must be paired up with LLIB_HEADER_START
	LLIB_SOURCEFILE		Just appends using namespace LLIB_NAMESPACE; to the top of a source file. DO NOT USE IN HEADERS
	FORCEINLINE			Ensure a function is always inline
	

*/
#pragma once

////////////////////////
// Standard library includes
#include <assert.h>
#include <memory>
////////////////////////

#define LLIB_VERSION "1.0"

#ifdef WIN32 || defined(WIN64)
	#define WINDOWS
#endif

#if defined(LLIB_EXPORT)
	#ifdef WIN32 || defined(WIN64)
		#define LLIB_API __declspec(dllexport)
		#define LLIB_HEADER_API 
		#define LLIB_MATH_API
		#define LLIB_MATH_API_ASM
	#else
		#define LLIB_API __attribute__((dllexport))
		#define LLIB_HEADER_API 
		#define LLIB_MATH_API
		#define LLIB_MATH_API_ASM
	#endif //WIN32 OR WIN64
#else
	#ifdef WIN32 || defined(WIN64)
		#define LLIB_API __declspec(dllimport)
		#define LLIB_HEADER_API 
		#define LLIB_MATH_API /*extern "C" __declspec(dllimport)*/
		#define LLIB_MATH_API_ASM extern "C" __declspec(dllimport)
	#else
		#define LLIB_API __attribute__((dllimport))
		#define LLIB_HEADER_API 
		#define LLIB_MATH_API /*extern "C" __attribute__((dllimport))*/
		#define LLIB_MATH_API_ASM extern "C" __attribute__((dllimport))
	#endif //WIN32 OR WIN64
#endif //WIN32 OR WIN64


//Debug specifications
#ifdef _DEBUG
	#define _llib_assert(cond) assert(cond)
	#define DEBUG_NOEXCEPT
#else
	#define _llib_assert(cond, msg)
	#define DEBUG_NOEXCEPT noexcept
#endif //_DEBUG

#define LLIB_ASSERT(cond) _llib_assert(cond)

#define LLIB_NAMESPACE LLib
#define LLIB_HEADER_START namespace LLib {
#define LLIB_HEADER_END }
#define LLIB_SOURCEFILE using namespace LLib;

#ifdef 0
#ifdef WINDOWS
#include <Windows.h>
#else
#include <sys.h>
#endif
#endif

//type definitions
typedef char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned int uint32;
typedef long long int64;
typedef unsigned long long uint64;
typedef void* ptr_t;
typedef void* __ptr32 ptr32_t;
typedef void* __ptr64 ptr64_t;

#undef FORCEINLINE
#define FORCEINLINE __forceinline


#ifdef 0
#ifdef WINDOWS
BOOL WINAPI DllMain( _In_ HINSTANCE hinstDLL, _In_ DWORD fdwReason, _In_ LPVOID lpvReserved);
#else
//none
#endif //WINDOWS
#endif

LLIB_HEADER_START

//Library entry point
void Init();

LLIB_HEADER_END