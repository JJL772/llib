/*


tan.h

Cosine implementations


*/
#pragma once

#include "LLib.h"

LLIB_HEADER_START

FORCEINLINE float m_fasttanf(float f);

FORCEINLINE double m_fasttan(double d);

FORCEINLINE float m_tanf(float f);

FORCEINLINE double m_tan(double d);

LLIB_HEADER_END