/*

Pointer.h

LLib Smart pointer library

*/
#pragma once

#include "LLib.h"

LLIB_HEADER_START

/*

SEMANTICS OVERVIEW



*/

/*

Pointer

A smart pointer class. 
This class will own the pointers you feed it and it will destroy them when it comes out of scope.
It is undefined behaviour if you delete the pointer yourself. In debug mode, an assertion is thrown if you delete the internal pointer.
You cannot reassign the pointer this references.
*/
template<class T>
class Pointer
{
private:
	T * m_ptr = NULL;

public:
	template<class ...Args>
	Pointer(Args... args)
	{
		m_ptr = new T(args...);
	}

	Pointer(T* ptr)
	{
		LLIB_ASSERT(m_ptr != NULL);
		m_ptr = ptr;
	}

	~Pointer()
	{
		LLIB_ASSERT(m_ptr != NULL);
		delete m_ptr;
	}

public:
	FORCEINLINE T* GetPointer() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE T& GetReference() const
	{
		LLIB_ASSERT(m_ptr);
		return *m_ptr;
	}

	FORCEINLINE T* Get() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE void Swap(Pointer<T>& other)
	{
		LLIB_ASSERT(m_ptr);
		LLIB_ASSERT(other.m_ptr);

		T* temp = other.m_ptr;
		other.m_ptr = this>m_ptr;
		this->m_ptr = temp;
	}

	FORCEINLINE T* operator*() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE T* operator->() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE bool operator==(const T* other) const
	{
		return m_ptr == other;
	}

	FORCEINLINE bool operator==(const Pointer<T>& other) const
	{
		return m_ptr == other.m_ptr;
	}

	FORCEINLINE bool operator!=(const T* other) const
	{
		return m_ptr != other;
	}

	FORCEINLINE bool operator!=(const Pointer<T>& other) const
	{
		return m_ptr != other.m_ptr;
	}

	FORCEINLINE Pointer<T>& operator=(const T* other) const
	{
		Pointer<T> _new(other);
		return _new;
	}
};

/*

ReferencePointer

Meant to be used with the GarbageCollector

*/
template<class T>
class ReferencePointer
{
private:

public:
};

LLIB_HEADER_END