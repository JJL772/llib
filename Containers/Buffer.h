/*

Buffer.h

Defines a buffer class

*/
#pragma once

#include "../LLib.h"

LLIB_HEADER_START

template<class T>
class Buffer
{
protected:
	uint32 Length = 0;

	T* Contents;

	uint32 Index = 0;

	uint32 InsertionIndex = 0;

public:
	Buffer() 
	{
	}

	Buffer(T* other, uint32 count)
	{
		std::memcpy((void*)Contents, (void*)other, sizeof(T) * count);
		Length = count;
	}

	Buffer(const Buffer<T>& other)
	{
		if (other.GetContents() != NULL) 
		{
			if (Length - Index < other.GetLength())
				Contents = (T*)std::realloc((void*)Contents, sizeof(Index + other.GetLength()));
			memcpy((void*)Contents, (void*)other.GetContents(), sizeof(T) * other.GetLength());
			InsertionIndex += other.GetLength();
		}
	}

	~Buffer()
	{
		
	}

	uint32 GetLength() const { return Length; }

	uint32 GetInsertionIndex() const { return InsertionIndex; }

	const T* GetContents() const { return Contents; }

	void Advance(uint32 count)
	{
		Index += count;
	}

	const T* Peek() const
	{
		if (Index < Length - 1)
			return &Contents[Index + 1];
		return NULL;
	}

	//Inserts the specified array of elements into the buffer and advances 
	void Insert(T* ammt, uint32 count)
	{

	}

	//Inserts the specified buffer into this buffer, reallocates as necessary 
	void Insert(const Buffer<T>& buf)
	{

	}

};

/*
Buffer of bytes!
*/
class ByteBuffer
{
private:
	char* m_buffer;
	int m_length;

public:
	ByteBuffer(const char* bytes, int len)
	{
		if (m_buffer)
			std::free(m_buffer);
		m_buffer = (char*)std::malloc(len);
		memcpy(m_buffer, bytes, len);
		m_length = len;
	}

	ByteBuffer(const ByteBuffer& other)
	{
		if (m_buffer)
			std::free(m_buffer);
		m_buffer = (char*)std::malloc(other.m_length);
		memcpy(m_buffer, other.m_buffer, other.m_length);
		this->m_length = other.m_length;
	}

	ByteBuffer(ByteBuffer&& other)
	{
		m_buffer = other.m_buffer;
		m_length = other.m_length;
		other.m_buffer = NULL;
		other.m_length = 0;
	}

	~ByteBuffer()
	{
		if (m_buffer)
			std::free(m_buffer);
	}

public:
	FORCEINLINE const char* GetData() const
	{
		return m_buffer;
	}

	FORCEINLINE int GetLength() const
	{
		return m_length;
	}

	FORCEINLINE char* Copy() const
	{
		if (!m_buffer && m_length != 0)
			return NULL;
		char* tmp = (char*)malloc(m_length);
		memcpy(tmp, m_buffer, m_length);
		return tmp;
	}

	FORCEINLINE void Dispose()
	{
		if (m_buffer)
			std::free(m_buffer);
		m_length = 0;
	}

	FORCEINLINE bool IsValid() const
	{
		return m_buffer != NULL;
	}

	FORCEINLINE bool Equals(const ByteBuffer& other)
	{
		for(int i = 0; i < m_length && i < other.m_length; i++)
		{
			if (other.m_buffer[i] != m_buffer[i])
				return false;
		}
		return other.m_length == m_length;
	}

	bool operator==(const ByteBuffer& other)
	{
		return Equals(other);
	}

	bool operator!=(const ByteBuffer& other)
	{
		return !Equals(other);
	}

	ByteBuffer& operator=(const ByteBuffer& other)
	{
		m_length = other.m_length;
		if (m_buffer)
			std::free(m_buffer);
		memcpy(m_buffer, other.m_buffer, m_length);
		return *this;
	}

	ByteBuffer& operator=(ByteBuffer&& other)
	{
		m_length = other.m_length;
		m_buffer = other.m_buffer;
		other.m_buffer = NULL;
		other.m_length = 0;
		return *this;
	}
};

LLIB_HEADER_END