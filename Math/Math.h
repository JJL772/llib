#pragma once

#include "LLib.h"

#ifndef _MATH_ASM
#include <intrin.h>
#include <xmmintrin.h>
#endif

#include "impl/cos.h"
#include "impl/sin.h"
#include "impl/tan.h"

//LLIB_API float  m_sqrtf(float f);
LLIB_API double m_sqrt(double d);

LLIB_MATH_API_ASM float		_m_sqrtf	(	float f		);
LLIB_MATH_API_ASM double	_m_sqrt		(	double d	);
LLIB_MATH_API_ASM float		_m_cosf		(	float f		);
LLIB_MATH_API_ASM double	_m_cos		(	double d	);
LLIB_MATH_API_ASM float		_m_sinf		(	float f		);
LLIB_MATH_API_ASM double	_m_sin		(	double d	);
LLIB_MATH_API_ASM float		_m_tanf		(	float f		);
LLIB_MATH_API_ASM double	_m_tan		(	double d	);
LLIB_MATH_API_ASM float		_m_absf		(	float f		);
LLIB_MATH_API_ASM double	_m_abs		(	double d	);
LLIB_MATH_API_ASM float		_m_rsqrtf	(	float f		);

LLIB_HEADER_START

//I like things large
constexpr const double PI = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862;

constexpr const double e = 2.718281828459045235360287471352662497757247093699959574966967627724076630353;

//NaN constants
constexpr const double DoubleNaN = 0x7FF0000000000001;

constexpr const float FloatNaN = 0x7F800001;

FORCEINLINE constexpr double factorial(uint32 x)
{
	if (x == 0)
		return 1;

	double ret = 1;

	for(; x >= 1; x--)
	{
		ret *= x;
	}

	return ret;
}

//Computes PI by n (PI/n1)
FORCEINLINE constexpr double PI_N(double n1)
{
	return LLib::PI / n1;
}

FORCEINLINE constexpr double divide(double n1, double n2)
{
	return n1 / n2;
}

//Computes e^n1
FORCEINLINE constexpr double E_X(double n1)
{
	return pow(LLib::e, n1);
}

FORCEINLINE float sqrtf(float f)
{
#ifdef _MATH_ASM
	return _m_sqrtf(f);
#else
	__m128 param = _mm_load_ps1(&f);

	param = _mm_sqrt_ps(param);

	return *reinterpret_cast<float*>(&param);

#endif //_MATH_ASM
}

FORCEINLINE double sqrt(double d)
{
#ifdef _MATH_ASM
	return _m_sqrt(d);
#else
	__m128d param = _mm_load1_pd(&d);

	param = _mm_sqrt_pd(param);

	return *reinterpret_cast<float*>(&param);
#endif
}

FORCEINLINE float cosf(float f)
{
#ifdef _MATH_ASM
	return _m_cosf(f);
#else

#ifdef _FAST_MATH
	return m_fastcosf(f);
#else
	return m_cosf(f);
#endif

#endif
}

FORCEINLINE double cos(double d)
{
#ifdef _MATH_ASM
	return _m_cos(d);
#else

#ifdef _FAST_MATH
	return m_fastcos(d);
#else
	return m_cos(d);
#endif

#endif
}

FORCEINLINE float sinf(float f)
{
#ifdef _MATH_ASM
	return _m_sinf(f);
#else

#ifdef _FAST_MATH
	return m_fastsinf(f);
#else
	return m_sinf(f);
#endif

#endif
}

FORCEINLINE double sin(double d)
{
#ifdef _MATH_ASM
	return _m_sin(d);
#else

#ifdef _FAST_MATH
	return m_fastsin(d);
#else
	return m_sin(d);
#endif

#endif
}

FORCEINLINE float tanf(float f)
{
#ifdef _MATH_ASM
	return _m_tanf(f);
#else

#ifdef _FAST_MATH
	return m_fasttanf(f);
#else
	return m_tanf(f);
#endif

#endif 
}

FORCEINLINE double tan(double d)
{
#ifdef _MATH_ASM
	return _m_tan(d);
#else

#ifdef _FAST_MATH
	return m_fasttan(d);
#else
	return m_tan(d);
#endif

#endif
}

FORCEINLINE float absf(float f)
{
#ifdef _MATH_ASM
	return _m_absf(f);
#else

#endif
}

FORCEINLINE double abs(double d)
{
#ifdef _MATH_ASM
	return _m_abs(d);
#else

#endif
}

FORCEINLINE float lerpf(float n0, float n1, float bias)
{
	return n0 + bias / 1.0f * (n1 - n0);
}

FORCEINLINE double lerp(double n0, double n1, double bias)
{
	return n0 + bias / 1.0 * (n1 - n0);
}

LLIB_HEADER_END