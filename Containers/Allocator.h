/*

Allocator.h

Defines the allocator class

*/
#pragma once

#include <stddef.h>
#include <cstdlib>

class IAllocator
{
public:
	virtual void* malloc(size_t size) const = 0;

	virtual void* realloc(void* block, size_t size) const = 0;

	virtual void free(void* block) const = 0;

	virtual void* calloc(size_t num, size_t size) const = 0;
};

/*
Default memory allocator
Does nothing except allocate and free memory.
*/
class DefaultAllocator : public IAllocator
{
public:
	virtual void* malloc(size_t size) const
	{
		return std::malloc(size);
	}

	virtual void* realloc(void* block, size_t size) const
	{
		return std::realloc(block, size);
	}

	virtual void free(void* block) const
	{
		return std::free(block);
	}

	virtual void* calloc(size_t num, size_t size) const
	{
		return std::calloc(num, size);
	}
};